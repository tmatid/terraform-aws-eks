# Variables Configuration

variable "cluster-name" {
  default     = "our-eks"
  type        = "string"
  description = "The name of your EKS Cluster"
}

variable "aws-region" {
  default     = "eu-west-1"
  type        = "string"
  description = "The AWS Region to deploy EKS"
}

variable "vpc-subnet-cidr" {
  default     = "10.0.0.0/16"
  type        = "string"
  description = "The VPC Subnet CIDR"
}
variable "node-instance-type" {
  default     = "t2.large"
  type        = "string"
  description = "Worker Node EC2 instance type"
}

variable "desired-capacity" {
  default     = 3
  type        = "string"
  description = "Autoscaling Desired node capacity"
}

variable "max-size" {
  default     = 5
  type        = "string"
  description = "Autoscaling maximum node capacity"
}

variable "min-size" {
  default     = 1
  type        = "string"
  description = "Autoscaling Minimum node capacity"
}

variable "ssh-key-name" {
  default     = "eu-west-1"
  type        = "string"
  description = "ssh key for the worker nodes"
}

variable "worker-public-ip" {
  default     = true
  description = "associate a public ip address on the worker nodes"
}

variable "worker-bootstrap-args" {
  default     = ""
  type        = "string" 
  description = "worker kubelet bootstrap /etc/eks/bootstrap.sh ${ClusterName} ${BootstrapArguments}"
}
